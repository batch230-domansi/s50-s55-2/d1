import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register(){

	const {user} = useContext(UserContext);

    //State hooks to store the values of input fields
    const[firstName, setfirstName] = useState('');
    const[lastName, setlastName] = useState('');
    const[mobileNumber, setMobileNumber] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);


    function registerUser(event){
        event.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: { 'Content-Type' : 'application/json'},
            body: JSON.stringify({
               firstName,
                lastName,
                email,
                password1,
                password2,
                mobileNumber
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            console.log("Check accessToken");
            console.log(data.accessToken);

            if(typeof data.accessToken !== "undefined"){
                localStorage.setItem('token' , data.accessToken);

                Swal.fire({
                    title: "Registration Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })   
            }
            else{
                Swal.fire({
                    title: "Duplicate email found",
                    icon: "error",
                    text: "Please provide a different email"
                })
            }


        }) 
    }

    useEffect(() => {
        if(firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '' && mobileNumber !== '' && mobileNumber.length >= 11){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [firstName, lastName, email, password1, password2, mobileNumber])

    // const { user, setUser } = useContext(UserContext);

    return(
        (user.id !== null)?
        <Navigate to ="/courses" />
        :
        <Form onSubmit={(event) => registerUser(event)}>
            <Form.Group controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter first name" 
                    value = {firstName}
                    onChange = {event => setfirstName(event.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                </Form.Text>
            </Form.Group>

             <Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter last name" 
                    value = {lastName}
                    onChange = {event => setlastName(event.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                </Form.Text>
            </Form.Group>

             <Form.Group controlId="mobileNumber">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="number" 
                    placeholder="Enter mobile number" 
                    value={mobileNumber}
                    onChange={event => setMobileNumber(event.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value = {email}
                    onChange = {event => setEmail(event.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value = {password1}
                    onChange = {event => setPassword1(event.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value = {password2}
                    onChange = {event => setPassword2(event.target.value)}
                    required
                />
            </Form.Group>

            <div className='pt-3'>
                { isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                        Submit
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                        Submit
                    </Button>
                }
            </div>
        </Form>
    )
}